FROM alpine:latest
RUN apk add --no-cache bash util-linux
ADD script-runner.sh /
CMD /script-runner.sh