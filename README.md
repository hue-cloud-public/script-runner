# Run an init script loaded from an env variable

On a Kubernetes cluster, to be ran as a per-node init script (similar to an initContainer but across nodes, i.e. a DaemonSet).

## Create a DaemonSet

```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: script-runner
  labels:
    k8s-app: script-runner
  namespace: huecloud
spec:
  selector:
    matchLabels:
      name: script-runner
  template:
    metadata:
      labels:
        name: script-runner
      namespace: huecloud
    spec:
      hostPID: true
      containers:
        - name: startup-script
          image: registry.gitlab.com/hue-cloud-public/script-runner
          imagePullPolicy: Always
          securityContext:
            privileged: true
          resources:
            limits:
              memory: 200Mi
            requests:
              cpu: 100m
              memory: 200Mi
          env:
          - name: STARTUP_SCRIPT
            value: |
              #! /bin/sh
              echo "Hi there!"
              ls -la /my-dir
          volumeMounts:
          - mountPath: /my-dir
            name: my-volume
      terminationGracePeriodSeconds: 30
      volumes:
      - name: my-volume
        hostPath:
          path: /var/my-dir
          type: DirectoryOrCreate

```