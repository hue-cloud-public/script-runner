#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

id

if [[ -z "${STARTUP_SCRIPT}" ]]; then
  echo "Missing mandatory env variable: STARTUP_SCRIPT"
  exit 1
fi

CHECKPOINT_PATH="${CHECKPOINT_PATH:-/tmp/script-checkpoint_$(md5sum <<<"${STARTUP_SCRIPT}" | cut -c-32)}"
INTERVAL_SECONDS="${INTERVAL_SECONDS:-30}"

run_script() {
  local err=0;

  bash -c "${STARTUP_SCRIPT}" && err=0 || err=$?
  if [[ ${err} != 0 ]]; then
    echo "Script failed! (${err})" 1>&2
    # return 1
  else 
    touch "${CHECKPOINT_PATH}"
    echo "Script succeeded!" 1>&2
    # return 0
  fi
}

while :; do
  stat "${CHECKPOINT_PATH}" > /dev/null 2>&1 && err=0 || err=$?
  if [[ ${err} != 0 ]]; then
    echo "Running script..."
    run_script
  fi

  sleep "${INTERVAL_SECONDS}"
done